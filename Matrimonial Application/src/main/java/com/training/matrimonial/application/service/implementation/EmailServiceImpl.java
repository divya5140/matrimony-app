package com.training.matrimonial.application.service.implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.service.EmailService;

import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService{
	
	@Value("${spring.mail.username}")
	private String username;
	
	private final JavaMailSender javaMailSender;

	@Override
	public ResponseDto sendEmail(String toEmail, String subject, String body) {
		
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
			messageHelper.setFrom(username);
			messageHelper.setTo(toEmail);
			messageHelper.setSubject(subject);
			messageHelper.setText(body);
			
			javaMailSender.send(mimeMessage);
		}catch (Exception e) {
			throw new RuntimeException();
		}
		return ResponseDto.builder().responseCode(200).message("Email sent successfully").build();
	}

}
