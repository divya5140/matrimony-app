package com.training.matrimonial.application.exception;

public class InvalidUserRequest extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidUserRequest(String message) {
		super(message);
	}

	public InvalidUserRequest() {
		super("Invalid input");
	}
}
