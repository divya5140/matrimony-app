package com.training.matrimonial.application.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.training.matrimonial.application.dto.SearchRequest;
import com.training.matrimonial.application.entity.Gender;
import com.training.matrimonial.application.entity.MartialStatus;
import com.training.matrimonial.application.entity.Profile;
import com.training.matrimonial.application.entity.Zodiac;
import com.training.matrimonial.application.repository.SearchRepository;
import com.training.matrimonial.application.service.SearchService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

	private final SearchRepository searchRepository;

	public List<Profile> searchProfile(SearchRequest dto) {

		return searchRepository
				.findByGenderAndAgeOrGenderAndLocationOrGenderAndProfessionOrGenderAndMartialStatusOrGenderAndCasteOrGenderAndReligionOrGenderAndSalaryOrGenderAndZodiacSign(
						Gender.valueOf(dto.getGender()), dto.getAge(), Gender.valueOf(dto.getGender()),
						dto.getLocation(), Gender.valueOf(dto.getGender()), dto.getProfession(),
						Gender.valueOf(dto.getGender()), MartialStatus.valueOf(dto.getMartialStatus()),
						Gender.valueOf(dto.getGender()), dto.getCaste(), Gender.valueOf(dto.getGender()),
						dto.getReligion(), Gender.valueOf(dto.getGender()), dto.getSalary(),
						Gender.valueOf(dto.getGender()), Zodiac.valueOf(dto.getZodiacSign()));
	}

}
