package com.training.matrimonial.application.entity;

public enum Zodiac {
	ARIES, TAURUS, GEMINI, CANCER, LEO, VIRGO, 
	LIBRA, SCORPIO, SAGITTARIUS, CAPRICORN, AQUARIUS, PISCES
}
