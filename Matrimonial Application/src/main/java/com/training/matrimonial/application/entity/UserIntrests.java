package com.training.matrimonial.application.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserIntrests {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long userInterstsId;

	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;

	private long usersIntresetId;

	@Enumerated(EnumType.STRING)
	private Interest intrest;

}