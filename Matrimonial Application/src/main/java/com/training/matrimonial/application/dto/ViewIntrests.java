package com.training.matrimonial.application.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ViewIntrests {

	private long userId;
	
	private List<IntrestsStatus> intrestsStatus;
}
