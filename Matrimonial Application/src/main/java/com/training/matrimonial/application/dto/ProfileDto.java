package com.training.matrimonial.application.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileDto {

	@NotEmpty(message = "ContactNo is required")
	private String contactNo;
	
	@NotEmpty(message = "dateOfBirth is required")
	private LocalDate dateOfBirth;
	
	@NotEmpty(message = "age is required")
	private int age;

	@NotEmpty(message = "gender is required")
	private String gender;

	@NotEmpty(message = "zodiacSign is required")
	private String zodiacSign;

	@NotEmpty(message = "religion is required")
	private String religion;

	
	@NotEmpty(message = "caste is required")
	private String caste;

	@NotEmpty(message = "martialStatus is required")
	private String martialStatus;

	@NotEmpty(message = "motherTongue is required")
	private String motherTongue;

	@NotEmpty(message = "profession is required")
	private String profession;

	@NotEmpty(message = "salary is required")
	private long salary;

	@NotEmpty(message = "education is required")
	private String education;
	
	@NotEmpty(message = "location is required")
	private String location;

}
