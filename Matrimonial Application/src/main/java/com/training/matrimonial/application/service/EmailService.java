package com.training.matrimonial.application.service;

import com.training.matrimonial.application.dto.ResponseDto;

public interface EmailService {
	
	ResponseDto sendEmail(String toEMail, String subject, String body);

}
