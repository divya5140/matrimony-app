package com.training.matrimonial.application.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.matrimonial.application.dto.IntrestDto;
import com.training.matrimonial.application.dto.IntrestsStatus;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.ViewIntrests;
import com.training.matrimonial.application.service.IntrestService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/intrests")
@RequiredArgsConstructor
public class IntrestController {

	private final IntrestService intrestService;

	@PostMapping
	public ResponseEntity<ResponseDto> addIntrests(@RequestBody IntrestDto intrestDto) {
		return new ResponseEntity<>(intrestService.addIntrests(intrestDto), HttpStatus.OK);
	}
	
	@GetMapping("/{userId}")
	public ResponseEntity<ViewIntrests> getIntrests(@PathVariable long userId) {
		return ResponseEntity.ok(intrestService.getIntrests(userId));
	}
	
	@PutMapping("/{userId}")
	public ResponseEntity<ResponseDto> updateIntrests(@PathVariable long userId, @RequestBody List<IntrestsStatus> status) {
		return ResponseEntity.ok(intrestService.updateIntrests(userId,status));
	}

}