package com.training.matrimonial.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.matrimonial.application.entity.Gender;
import com.training.matrimonial.application.entity.MartialStatus;
import com.training.matrimonial.application.entity.Profile;
import com.training.matrimonial.application.entity.Zodiac;

public interface SearchRepository extends JpaRepository<Profile, Long> {

	List<Profile> findByGenderAndAgeOrGenderAndLocationOrGenderAndProfessionOrGenderAndMartialStatusOrGenderAndCasteOrGenderAndReligionOrGenderAndSalaryOrGenderAndZodiacSign(
			Gender gender,int age,Gender gender1,String location,Gender gender2,String profession,
			Gender gender3,MartialStatus martialStatus,Gender gender4,String caste,Gender gender5,
			String religion,Gender gender6, long salary,Gender gender7,Zodiac zodiacSign );
}
