package com.training.matrimonial.application.exception;

import java.util.List;

public class ErrorResponse {
	private long errorCode;
	private List<String> errorResponses;

	public ErrorResponse(long errorCode, List<String> errorResponses) {
		super();
		this.errorCode = errorCode;
		this.errorResponses = errorResponses;
	}

	public long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(long errorCode) {
		this.errorCode = errorCode;
	}

	public List<String> getErrorResponses() {
		return errorResponses;
	}

	public void setErrorResponses(List<String> errorResponses) {
		this.errorResponses = errorResponses;
	}

}
