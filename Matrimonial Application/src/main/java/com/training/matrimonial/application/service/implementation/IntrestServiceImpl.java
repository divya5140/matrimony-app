package com.training.matrimonial.application.service.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.training.matrimonial.application.dto.IntrestDto;
import com.training.matrimonial.application.dto.IntrestsStatus;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.ViewIntrests;
import com.training.matrimonial.application.entity.Interest;
import com.training.matrimonial.application.entity.LoginStatus;
import com.training.matrimonial.application.entity.User;
import com.training.matrimonial.application.entity.UserIntrests;
import com.training.matrimonial.application.exception.InvalidUserRequest;
import com.training.matrimonial.application.exception.UnAuthorizedAccess;
import com.training.matrimonial.application.exception.UserNotFoundException;
import com.training.matrimonial.application.repository.UserIntrestRepository;
import com.training.matrimonial.application.repository.UserRepository;
import com.training.matrimonial.application.service.IntrestService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class IntrestServiceImpl implements IntrestService {

	private final UserRepository userRepository;

	private final UserIntrestRepository intrestRepository;

	private List<UserIntrests> verifyIntrests(long userId) {

		User intrestUser = userRepository.findById(userId).orElseThrow(() -> {
			log.warn("Invalid user id");
			return new UserNotFoundException("User not found");
		});
		if (!intrestUser.getProfile().getLoginStatus().equals(LoginStatus.LOGIN)) {
			log.warn("Unautjorized Access: You need to login");
			throw new UnAuthorizedAccess("you need to Login");
		}

		List<UserIntrests> intrests = intrestRepository.findByUsersIntresetId(userId);
		if (intrests.isEmpty()) {
			log.warn("No user have added you as their intrests");
			throw new UserNotFoundException("No user have added you as their intrests");
		}
		return intrests;
	}

	public ResponseDto addIntrests(IntrestDto intrests) {

		User intrestUser = userRepository.findById(intrests.getUserId()).orElseThrow(() -> {
			log.warn("Invalid user id");
			return new UserNotFoundException("User not found");
		});
		if (!intrestUser.getProfile().getLoginStatus().equals(LoginStatus.LOGIN)) {

			log.warn("Unautjorized Access: You need to login");
			throw new UnAuthorizedAccess("you need to Login, to add profiles to your favourites");
		}
		
		if (intrests.getUserIds().stream().anyMatch(user -> user.equals(intrestUser.getUserId()))) {
			log.warn("You cannot add your self as favourites");
			throw new InvalidUserRequest("You cannot add your self as favourites");
		}

		log.info("Requested user found");
		List<User> users = userRepository.findAllById(intrests.getUserIds());
		Map<Long, User> userMap = users.stream().collect(Collectors.toMap(User::getUserId, user -> user));
		List<UserIntrests> userIntrests = new ArrayList<>();
		intrests.getUserIds().forEach(intrest -> {
			if (Objects.isNull(userMap.get(intrest))) {
				log.warn("User not found");
				throw new UserNotFoundException("Requested user's intrested profile not found");
			}
			userIntrests.add(
					UserIntrests.builder().intrest(Interest.REJECT).user(intrestUser).usersIntresetId(intrest).build());
		});
		intrestRepository.saveAll(userIntrests);
		return new ResponseDto(200, "Intrests added successfully");

	}

	@Override
	public ViewIntrests getIntrests(long userId) {

		List<UserIntrests> intrests = verifyIntrests(userId);
		
		List<IntrestsStatus> status = intrests.stream().map(intrest -> IntrestsStatus.builder()
				.interest(intrest.getIntrest().toString()).usersFavouriteId(intrest.getUser().getUserId()).build())
				.toList();

		return ViewIntrests.builder().intrestsStatus(status).userId(userId).build();
	}

	@Override
	public ResponseDto updateIntrests(long userId, List<IntrestsStatus> status) {

		List<UserIntrests> intrests = verifyIntrests(userId);
		
		Map<Long, UserIntrests> statusMap = intrests.stream().collect(
				Collectors.toMap(userIntrest -> userIntrest.getUser().getUserId(), userIntrest -> userIntrest));

		status.forEach(intrestStatus -> {
			if (Objects.isNull(statusMap.get(intrestStatus.getUsersFavouriteId()))) {
				log.warn("You are trying to accept the user who has not added you as favourite");
				throw new UserNotFoundException("You are trying to accept the user who has not added you as favourite");
			}
			statusMap.get(intrestStatus.getUsersFavouriteId())
					.setIntrest(Interest.valueOf(intrestStatus.getInterest()));
		});

		intrestRepository.saveAll(statusMap.values().stream().toList());
		log.info("Status updated successfully");
		return ResponseDto.builder().responseCode(200).message("Status updated successfully").build();

	}

}
