package com.training.matrimonial.application.service;

import java.util.List;

import com.training.matrimonial.application.dto.SearchRequest;
import com.training.matrimonial.application.entity.Profile;

public interface SearchService {

	List<Profile> searchProfile(SearchRequest dto);

}
