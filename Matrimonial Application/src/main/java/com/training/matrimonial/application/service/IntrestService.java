package com.training.matrimonial.application.service;

import java.util.List;

import com.training.matrimonial.application.dto.IntrestDto;
import com.training.matrimonial.application.dto.IntrestsStatus;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.ViewIntrests;

public interface IntrestService {

	ResponseDto addIntrests(IntrestDto intrests);

	ViewIntrests getIntrests(long userId);

	ResponseDto updateIntrests(long userId, List<IntrestsStatus> status);
}
