package com.training.matrimonial.application.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.matrimonial.application.dto.SearchRequest;
import com.training.matrimonial.application.entity.Profile;
import com.training.matrimonial.application.service.SearchService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class SearchController {
	
	
	private final SearchService searchService;

	@PostMapping("/profiles")
	public ResponseEntity<List<Profile>> searchProfile(@RequestBody @Valid SearchRequest dto) {
		return ResponseEntity.ok(searchService.searchProfile(dto));
	}

}
