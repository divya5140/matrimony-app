package com.training.matrimonial.application.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.matrimonial.application.entity.User;


public interface UserRepository extends JpaRepository<User, Long> {

	User findByemailIdAndPassword(String emailId, String password);
	
	Optional<User> findByEmailId(String emailId);

}
