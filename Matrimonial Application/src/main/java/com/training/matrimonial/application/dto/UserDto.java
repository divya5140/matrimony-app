package com.training.matrimonial.application.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {

	@NotEmpty(message = "Name is required")
	@Pattern(regexp = "[a-zA-Z ]+", message = "Name should contain only alphabets")
	private String name;
	
	@NotEmpty(message = "COntact number is required")
	@Pattern(regexp = "[6-9]{1}[0-9]{9}")
	private String contactNo;

	@Email
	@NotEmpty(message = "EmailId is required")
	@Pattern(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
	private String emailId;

	@NotEmpty(message = "Password is required")
	private String password;

	@NotEmpty(message = "ReType the password")
	private String confirmPassword;

}
