package com.training.matrimonial.application.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Profile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long profileId;
	
	
	
	private LocalDate dateOfBirth;
	
	private int age;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	@Enumerated(EnumType.STRING)
	private Zodiac zodiacSign;
	
	private String religion;
	
	private String caste;
	
	@Enumerated(EnumType.STRING)
	private MartialStatus martialStatus;
	
	private String motherTongue;
	
	private String profession;
	
	private long salary;
	
	private String education;
	
	private String location;
	
	private LoginStatus loginStatus;
}
