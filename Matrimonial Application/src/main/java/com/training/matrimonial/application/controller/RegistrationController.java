package com.training.matrimonial.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.matrimonial.application.dto.LoginDto;
import com.training.matrimonial.application.dto.ProfileDto;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.UserDto;
import com.training.matrimonial.application.service.UserService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class RegistrationController {

	private final UserService userService;

	@PostMapping("/users")
	public ResponseEntity<ResponseDto> addUser(@RequestBody @Valid UserDto userDto) {
		return new ResponseEntity<>(userService.addUser(userDto), HttpStatus.CREATED);
	}

	@PutMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@Valid @RequestBody LoginDto loginDto) {
		return new ResponseEntity<>(userService.login(loginDto), HttpStatus.CREATED);
	}

	@PutMapping("/logout")
	public ResponseEntity<ResponseDto> userLogout(@Valid @RequestBody LoginDto logoutDto) {
		return new ResponseEntity<>(userService.logout(logoutDto), HttpStatus.CREATED);
	}

	@PutMapping("/profiles/{userId}")
	public ResponseEntity<ResponseDto> userProfile(@PathVariable long userId, @RequestBody ProfileDto profileDto) {
		return new ResponseEntity<>(userService.updateProfile(userId, profileDto), HttpStatus.OK);
	}

}
