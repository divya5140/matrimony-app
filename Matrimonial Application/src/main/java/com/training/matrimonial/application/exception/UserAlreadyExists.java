package com.training.matrimonial.application.exception;

public class UserAlreadyExists extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public UserAlreadyExists(String message) {
		super(message);
	}

	public UserAlreadyExists() {
		super("User already exists");
	}
	
	

}
