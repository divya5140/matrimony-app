package com.training.matrimonial.application.exception;

public class UnAuthorizedAccess extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnAuthorizedAccess(String message) {
		super(message);
	}

	public UnAuthorizedAccess() {
		super("Unauthorized access");
	}
}