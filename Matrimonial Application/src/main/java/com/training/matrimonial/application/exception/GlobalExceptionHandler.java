package com.training.matrimonial.application.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {

		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse response = new ErrorResponse(400L, details);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IncorrectPasswordException.class)
	public ResponseEntity<Object> departmentNotFoundException(IncorrectPasswordException ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse(404l, errorMessage), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(UnAuthorizedAccess.class)
	public ResponseEntity<Object> unAuthorizedAccess(UnAuthorizedAccess ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse(400l, errorMessage), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> userNotFoundException(UserNotFoundException ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse(404l, errorMessage), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(PasswordDoseNotMatchException.class)
	public ResponseEntity<Object> passwordDoseNotMatchException(PasswordDoseNotMatchException ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse(400l, errorMessage), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(InvalidUserRequest.class)
	public ResponseEntity<Object> invalidUserRequest(InvalidUserRequest ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse(400l, errorMessage), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UserAlreadyExists.class)
	public ResponseEntity<Object> userAlreadyExists(UserAlreadyExists ex, WebRequest request) {

		List<String> errorMessage = new ArrayList<>();
		errorMessage.add(ex.getLocalizedMessage());
		return new ResponseEntity<>(new ErrorResponse(409l, errorMessage), HttpStatus.CONFLICT);
	}
}