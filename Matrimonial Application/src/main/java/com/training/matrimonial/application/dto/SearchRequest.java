package com.training.matrimonial.application.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchRequest {
	
	private int age;
	
	@NotNull
	@Pattern(regexp = "(MALE|FEMALE)", message = "Gender can be either MALE or FEMALE")
	private String gender;
	
	@NotNull
	@Pattern(regexp = "(ARIES|TAURUS|GEMINI|CANCER|LEO|VIRGO|LIBRA|SCORPIO|SAGITTARIUS|CAPRICORN|AQUARIUS|PISCES)",
	message = "Enter a valid Zodiac Sign in Uppercase")
	private String zodiacSign;

	private String religion;

	private String caste;
	
	@NotNull
	@Pattern(regexp = "(SINGLE|DIVORCED|WIDOW|WIDOWER)", message = "Enter a valid martial status")
	private String martialStatus;

	private String profession;

	private long salary;

	private String location;
}
