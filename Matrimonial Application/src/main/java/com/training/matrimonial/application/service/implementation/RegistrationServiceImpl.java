package com.training.matrimonial.application.service.implementation;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.training.matrimonial.application.dto.LoginDto;
import com.training.matrimonial.application.dto.ProfileDto;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.UserDto;
import com.training.matrimonial.application.entity.Gender;
import com.training.matrimonial.application.entity.LoginStatus;
import com.training.matrimonial.application.entity.MartialStatus;
import com.training.matrimonial.application.entity.Profile;
import com.training.matrimonial.application.entity.User;
import com.training.matrimonial.application.entity.Zodiac;
import com.training.matrimonial.application.exception.PasswordDoseNotMatchException;
import com.training.matrimonial.application.exception.UnAuthorizedAccess;
import com.training.matrimonial.application.exception.UserAlreadyExists;
import com.training.matrimonial.application.exception.UserNotFoundException;
import com.training.matrimonial.application.repository.UserRepository;
import com.training.matrimonial.application.service.EmailService;
import com.training.matrimonial.application.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements UserService {

	private final UserRepository userRepository;
	
	//private final EmailService emailService;

	Logger logger = LoggerFactory.getLogger(getClass());

	public ResponseDto addUser(UserDto userDto) {
		
		Optional<User> user = userRepository.findByEmailId(userDto.getEmailId());
		if(user.isPresent()) {
			throw new UserAlreadyExists("User already exists");
		}
		if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
			logger.error("Password dose not match");
			throw new PasswordDoseNotMatchException("Password dose not match");
		}
		
		User newUser = new User();
		Profile profile = new Profile();
		BeanUtils.copyProperties(userDto, newUser);
		profile.setLoginStatus(LoginStatus.LOGOUT);
		//emailService.sendEmail("psavantravva@gmail.com", "Welcome: Matrimonial Application", "You have registered successfully, Please Login");
		newUser.setProfile(profile);
		userRepository.save(newUser);
		logger.info("User registered successfully");
		return new ResponseDto(200, "User registered successfully");

	}

	@Override
	public ResponseDto login(LoginDto loginDto) {

		User user = userRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword());
		if (user != null) {
			user.getProfile().setLoginStatus(LoginStatus.LOGIN);
			userRepository.save(user);
		} else {
			logger.error("Invalid Credentials or user Not found");
			throw new UnAuthorizedAccess("Invalid Credentials");
		}
		logger.info("User Logged in successfully");
		return new ResponseDto(200, "Login successful");
	}

	public ResponseDto logout(LoginDto logoutDto) {

		User user = userRepository.findByemailIdAndPassword(logoutDto.getEmailId(), logoutDto.getPassword());
		if (user != null) {
			user.getProfile().setLoginStatus(LoginStatus.LOGOUT);
			userRepository.save(user);
		} else {
			logger.error("Invalid Credentials or user Not found");
			throw new UserNotFoundException("Invalid Credentials or User Not Found");
		}
		logger.info("User Logged out successfully");
		return new ResponseDto(200, "Logout successful");
	}

	public ResponseDto updateProfile(long userId, ProfileDto profileDto) {
		
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new UserNotFoundException("User with userId: " + userId + " not found"));
		
		if (user.getProfile().getLoginStatus().equals(LoginStatus.LOGOUT)) {
			throw new UnAuthorizedAccess("You need to login to update the profile");
		}
		Profile profile = user.getProfile();
		BeanUtils.copyProperties(profileDto, profile);
		profile.setMartialStatus(MartialStatus.valueOf(profileDto.getMartialStatus()));
		profile.setGender(Gender.valueOf(profileDto.getGender()));
		profile.setZodiacSign(Zodiac.valueOf(profileDto.getZodiacSign()));

		user.setProfile(profile);
		userRepository.save(user);
		logger.info("Profile updated successfully");
		return new ResponseDto(200, "Profile updated successfully");

	}

}
