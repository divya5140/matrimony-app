package com.training.matrimonial.application.service;

import com.training.matrimonial.application.dto.LoginDto;
import com.training.matrimonial.application.dto.ProfileDto;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.UserDto;

public interface UserService {
	
	ResponseDto addUser(UserDto userDto);

	ResponseDto login(LoginDto loginDto);

	ResponseDto logout(LoginDto logoutDto);

	ResponseDto updateProfile(long userId, ProfileDto profileDto);
}
