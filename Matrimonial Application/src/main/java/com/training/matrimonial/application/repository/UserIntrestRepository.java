package com.training.matrimonial.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.matrimonial.application.entity.User;
import com.training.matrimonial.application.entity.UserIntrests;

public interface UserIntrestRepository extends JpaRepository<UserIntrests, Long>{

	List<UserIntrests> findByUsersIntresetId(long usersIntresetId);
	
	List<UserIntrests> findByUser(User user);
}
