package com.training.matrimonial.application.exception;

public class PasswordDoseNotMatchException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PasswordDoseNotMatchException(String message) {
		super(message);
	}
	
	public PasswordDoseNotMatchException() {
		super("Password dose not match");
	}
	
	

}
