package com.training.matrimonial.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.matrimonial.application.dto.IntrestDto;
import com.training.matrimonial.application.dto.IntrestsStatus;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.ViewIntrests;
import com.training.matrimonial.application.entity.Interest;
import com.training.matrimonial.application.entity.LoginStatus;
import com.training.matrimonial.application.entity.Profile;
import com.training.matrimonial.application.entity.User;
import com.training.matrimonial.application.entity.UserIntrests;
import com.training.matrimonial.application.exception.InvalidUserRequest;
import com.training.matrimonial.application.exception.UnAuthorizedAccess;
import com.training.matrimonial.application.exception.UserNotFoundException;
import com.training.matrimonial.application.repository.UserIntrestRepository;
import com.training.matrimonial.application.repository.UserRepository;

@ExtendWith(SpringExtension.class)
class IntrestServiceImplTest {

	@InjectMocks
	private IntrestServiceImpl intrestService;

	@Mock
	private UserRepository userRepository;

	@Mock
	private UserIntrestRepository intrestRepository;

	@Test
	void addIntrests_UserNotFound() {

		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		IntrestDto dto = IntrestDto.builder().userId(1).build();

		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> intrestService.addIntrests(dto));

		assertEquals("User not found", exception.getLocalizedMessage());
	}

	@Test
	void addIntrests_UserNotLoggedIn() {

		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGOUT).build()).build();

		IntrestDto dto = IntrestDto.builder().userIds(List.of(5l, 4l)).userId(2).build();

		Mockito.when(userRepository.findById(dto.getUserId())).thenReturn(Optional.of(user));

		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class, () -> intrestService.addIntrests(dto));

		assertEquals("you need to Login, to add profiles to your favourites", exception.getLocalizedMessage());
	}

	@Test
	void addIntrests_SelfFavourites() {

		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		IntrestDto dto = IntrestDto.builder().userIds(List.of(2l, 4l)).userId(2).build();

		Mockito.when(userRepository.findById(dto.getUserId())).thenReturn(Optional.of(user));

		List<User> users = List.of(User.builder().userId(2).build(), User.builder().userId(4).build());
		Mockito.when(userRepository.findAllById(dto.getUserIds())).thenReturn(users);

		InvalidUserRequest exception = assertThrows(InvalidUserRequest.class, () -> intrestService.addIntrests(dto));

		assertEquals("You cannot add your self as favourites", exception.getLocalizedMessage());
	}

	@Test
	void addIntrests_UsersIntrestNotFound() {

		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		IntrestDto dto = IntrestDto.builder().userIds(List.of(5l, 4l)).userId(2).build();

		Mockito.when(userRepository.findById(dto.getUserId())).thenReturn(Optional.of(user));

		List<User> users = List.of(User.builder().userId(1).build(), User.builder().userId(4).build());
		Mockito.when(userRepository.findAllById(dto.getUserIds())).thenReturn(users);

		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> intrestService.addIntrests(dto));

		assertEquals("Requested user's intrested profile not found", exception.getLocalizedMessage());
	}

	@Test
	void addIntrests_Success() {

		User user = User.builder().userId(1).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		IntrestDto dto = IntrestDto.builder().userIds(List.of(2l, 3l)).userId(1).build();

		Mockito.when(userRepository.findById(dto.getUserId())).thenReturn(Optional.of(user));

		List<User> users = List.of(User.builder().userId(2).build(), User.builder().userId(3).build());
		Mockito.when(userRepository.findAllById(dto.getUserIds())).thenReturn(users);

		ResponseDto response = intrestService.addIntrests(dto);
		assertNotNull(response);
	}

	@Test
	void getIntrests_UserNotFound() {

		long userId = 1;
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> intrestService.getIntrests(userId));

		assertEquals("User not found", exception.getLocalizedMessage());
	}

	@Test
	void getIntrests_UserNotLoggedIn() {

		long userId = 2;
		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGOUT).build()).build();

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class, () -> intrestService.getIntrests(userId));

		assertEquals("you need to Login", exception.getLocalizedMessage());
	}

	@Test
	void getIntrests_NoIntrests() {

		long userId = 2;
		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		Mockito.when(intrestRepository.findByUsersIntresetId(userId)).thenReturn(new ArrayList<>());

		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> intrestService.getIntrests(userId));

		assertEquals("No user have added you as their intrests", exception.getLocalizedMessage());
	}

	@Test
	void getIntrests_Success() {

		long userId = 2;
		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		List<UserIntrests> userIntrests = List.of(
				UserIntrests.builder().user(user).intrest(Interest.ACCEPT).userInterstsId(3).build(),
				UserIntrests.builder().user(user).intrest(Interest.REJECT).userInterstsId(4).build());

		Mockito.when(intrestRepository.findByUsersIntresetId(userId)).thenReturn(userIntrests);

		ViewIntrests intrests = intrestService.getIntrests(userId);
		assertNotNull(intrests);
		assertEquals(userId, intrests.getUserId());

	}
	 
	@Test
	void updateIntrests_UserNotFound() {
	
		long userId = 2;
		Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

		List<IntrestsStatus> status = new ArrayList<>();
		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> intrestService.updateIntrests(userId,status));

		assertEquals("User not found", exception.getLocalizedMessage());
	}
	
	@Test
	void updateIntrests_UserNotLoggedIn() {
	
		long userId = 2;
		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGOUT).build()).build();

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		List<IntrestsStatus> status = new ArrayList<>();
		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class,
				() -> intrestService.updateIntrests(userId, status));

		assertEquals("you need to Login", exception.getLocalizedMessage());
	}
	
	@Test
	void updateIntrests_NoIntrests() {

		long userId = 2;
		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		List<IntrestsStatus> status = new ArrayList<>();
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		Mockito.when(intrestRepository.findByUsersIntresetId(userId)).thenReturn(new ArrayList<>());

		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> intrestService.updateIntrests(userId, status));

		assertEquals("No user have added you as their intrests", exception.getLocalizedMessage());
	}
	
	@Test
	void updateIntrests_UpdatingUser_NotIntrest() {
	
		long userId = 2;
		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		List<IntrestsStatus> status = List.of(
				IntrestsStatus.builder().usersFavouriteId(3).build());
		
		List<UserIntrests> userIntrests = List.of(
				UserIntrests.builder().user(user).intrest(Interest.REJECT).userInterstsId(4).build());
		
		Mockito.when(intrestRepository.findByUsersIntresetId(userId)).thenReturn(userIntrests);

		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> intrestService.updateIntrests(userId, status));

		assertEquals("You are trying to accept the user who has not added you as favourite", exception.getLocalizedMessage());
	}
	
	@Test
	void updateIntrests_Success() {
		
		long userId = 2;
		User user = User.builder().userId(2).name("Vikram")
				.profile(Profile.builder().loginStatus(LoginStatus.LOGIN).build()).build();

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		List<IntrestsStatus> status = List.of(
				IntrestsStatus.builder().usersFavouriteId(3).interest("ACCEPT").build());
		
		List<UserIntrests> userIntrests = List.of(
				UserIntrests.builder().user(User.builder().userId(3).build()).intrest(Interest.REJECT).userInterstsId(4).build());
		
		Mockito.when(intrestRepository.findByUsersIntresetId(userId)).thenReturn(userIntrests);
		ResponseDto response = intrestService.updateIntrests(userId, status);
		
		assertNotNull(response);
		
	}
	
	
	
	
	

}
