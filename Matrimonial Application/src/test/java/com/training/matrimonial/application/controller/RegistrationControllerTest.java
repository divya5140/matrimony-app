package com.training.matrimonial.application.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.UserDto;
import com.training.matrimonial.application.service.UserService;

@ExtendWith(SpringExtension.class)
class RegistrationControllerTest {
	
	@Mock
	private UserService userService;
	
	@InjectMocks
	private RegistrationController registrationController;
	
    @Test
    void testAddUser_Positive() {
        UserDto userDto = new UserDto();
        ResponseDto expectedResponse = new ResponseDto();
        when(userService.addUser(userDto)).thenReturn(expectedResponse);
 
        ResponseEntity<ResponseDto> responseEntity = registrationController.addUser(userDto);
 
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

}
