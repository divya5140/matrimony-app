package com.training.matrimonial.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.matrimonial.application.dto.LoginDto;
import com.training.matrimonial.application.dto.ProfileDto;
import com.training.matrimonial.application.dto.ResponseDto;
import com.training.matrimonial.application.dto.UserDto;
import com.training.matrimonial.application.entity.LoginStatus;
import com.training.matrimonial.application.entity.MartialStatus;
import com.training.matrimonial.application.entity.Profile;
import com.training.matrimonial.application.entity.User;
import com.training.matrimonial.application.exception.IncorrectPasswordException;
import com.training.matrimonial.application.exception.PasswordDoseNotMatchException;
import com.training.matrimonial.application.exception.UnAuthorizedAccess;
import com.training.matrimonial.application.exception.UserAlreadyExists;
import com.training.matrimonial.application.exception.UserNotFoundException;
import com.training.matrimonial.application.repository.UserRepository;

@ExtendWith(SpringExtension.class)
class RegistrationServiceImplTest {

	@InjectMocks
	private RegistrationServiceImpl registrationService;

	@Mock
	private UserRepository userRepository;

	@Test
	void addUser_PositiveTest_IncorrectPassword() {
		User user = new User();
		UserDto userDto = new UserDto();
		userDto.setPassword("divya");
		userDto.setConfirmPassword("divya");
		if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
			throw new IncorrectPasswordException("Your Password is incorrect");
		}
		user.setPassword(userDto.getPassword());
		Mockito.when(userRepository.save(user)).thenReturn(user);
		ResponseDto response = registrationService.addUser(userDto);
		assertNotNull(response);
		assertEquals(200, response.getResponseCode());
		assertEquals("User registered successfully", response.getMessage());
	}

	@Test
	void addUser_NegativeTest_IncorrectPassword() {
		UserDto userDto = new UserDto();
		userDto.setPassword("divya");
		userDto.setConfirmPassword("bhavya");

		PasswordDoseNotMatchException exception = assertThrows(PasswordDoseNotMatchException.class,
				() -> registrationService.addUser(userDto));
		assertEquals("Password dose not match", exception.getMessage());

	}
	
	@Test
	void addUser_UserAlreadyExists() {
		
		User user = User.builder()
				.name("Vikram").emailId("vikram@gmail.com").build();
		
		UserDto userDto = UserDto.builder()
				.emailId("Vikram@gmail.com").password("Vikram@12324").confirmPassword("Vikram@12324").build();
		
		Mockito.when(userRepository.findByEmailId(userDto.getEmailId())).thenReturn(Optional.of(user));

		UserAlreadyExists exception = assertThrows(UserAlreadyExists.class,
				() -> registrationService.addUser(userDto));
		
		assertEquals("User already exists", exception.getMessage());

	}

	@Test
	void login_PositiveTest() {
		
		User user = new User();
		user.setProfile(new Profile());
		LoginDto loginDto = new LoginDto();
		when(userRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword())).thenReturn(user);
		ResponseDto response = registrationService.login(loginDto);
		assertNotNull(response);
		assertEquals(200, response.getResponseCode());
		assertEquals("Login successful", response.getMessage());
	}

	@Test
	void login_NegativeTest_UserNotFound() {
		LoginDto loginDto = new LoginDto();
		loginDto.setEmailId("divya.com");
		loginDto.setPassword("divya14");
		when(userRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword())).thenReturn(null);

		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class,
				() -> registrationService.login(loginDto));
		assertEquals("Invalid Credentials", exception.getMessage());

	}

	@Test
	void updateProfile_NegativeTest_UserNotFound() {
		long userId = 1L;
		ProfileDto profileDto = new ProfileDto();
		when(userRepository.findById(userId)).thenReturn(java.util.Optional.empty());
		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> registrationService.updateProfile(userId, profileDto));
		assertEquals("User with userId: " + userId + " not found", exception.getMessage());
	}
	
	@Test
	void testLogout_UserNotFound() {
		LoginDto loginDto = new LoginDto();
		loginDto.setEmailId("divya.com");
		loginDto.setPassword("divya14");
		when(userRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword())).thenReturn(null);

		UserNotFoundException exception = assertThrows(UserNotFoundException.class,
				() -> registrationService.logout(loginDto));
		assertEquals("Invalid Credentials or User Not Found", exception.getMessage());
		
	}
	
	@Test
	void testLogout_Success() {
		
		LoginDto loginDto = new LoginDto();
		loginDto.setEmailId("divya.com");
		loginDto.setPassword("divya14");
		
		User user = new User();
		user.setUserId(1l);
		user.setName("divya");
		Profile profile = Profile.builder().loginStatus(LoginStatus.LOGOUT).martialStatus(MartialStatus.SINGLE).build();
		user.setProfile(profile);
		when(userRepository.findByemailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword())).thenReturn(user);

		ResponseDto response = registrationService.logout(loginDto);
		assertNotNull(response);
		assertEquals(200, response.getResponseCode());
		assertEquals("Logout successful", response.getMessage());
	}
	
	@Test
	void updateProfile_UserNotLoggedIn() {

		long userId = 1L;
		User user = new User();
		user.setUserId(userId);
		user.setName("divya");
		Profile profile = Profile.builder().loginStatus(LoginStatus.LOGOUT).martialStatus(MartialStatus.SINGLE).build();
		user.setProfile(profile);
		when(userRepository.findById(userId)).thenReturn(java.util.Optional.of(user));
		
		ProfileDto profileDto = ProfileDto.builder().age(34).martialStatus("SINGLE").gender("MALE").zodiacSign("ARIES").build();
		
		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class, 
				() -> registrationService.updateProfile(userId, 
						profileDto));
		assertEquals("You need to login to update the profile", exception.getLocalizedMessage());
	}

	@Test
	void updateProfile_PositiveTest() {

		long userId = 1L;
		User user = new User();
		user.setUserId(userId);
		user.setName("divya");
		Profile profile = Profile.builder().loginStatus(LoginStatus.LOGIN).martialStatus(MartialStatus.SINGLE).build();
		user.setProfile(profile);
		when(userRepository.findById(userId)).thenReturn(java.util.Optional.of(user));
		ResponseDto response = registrationService.updateProfile(userId,
				ProfileDto.builder().age(34).martialStatus("SINGLE").gender("MALE").zodiacSign("ARIES").build());
		
		assertNotNull(response);
		assertEquals(200, response.getResponseCode());
		assertEquals("Profile updated successfully", response.getMessage());
	}

}
